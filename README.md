# GenAPoPop 1.0
Genetic analyses of Polyploid populations.

GenAPoPop 1.0 is a user-friendly software for easily compute genetic analyses of autopolyploid populations packaged for Linux, MacOS and Windows.

CC-BY-NC-SA license, version 4: Solenn Stoeckel, INRAE, 2022.

Stoeckel, S., Becheler, R., Bocharova, E., & Barloy, D. (2023). GenAPoPop 1.0: A user-friendly software to analyse genetic diversity and structure from partially clonal and selfed autopolyploid organisms. Molecular Ecology Resources, 00, 1–11. https://doi.org/10.1111/1755-0998.13886 

# How to use
1. Download the packaged file for your operating system.
* Linux: GenAPoPop1.0_linux.zip 
* MacOS (Darwin): GenAPoPop1.0_macos.zip 
* windows (10 & 11): GenAPoPop1.0_win.zip   
2. Unzip the downloaded archive in a folder where you have permission to write and execute files.
3. Change permission if needed. In Gnu-Linux system, right-click on the executable file and in the tab 'permission', tick 'allow execute file', or in command-line, enter _chmod o+rwx 2022-11-04_GenAPoPop1.0_.
4. Launch the GenAPoPop executable file (exe for windows, App for MacOS and x-executable for Linux)
5. A window will open allowing you to simulate or analyse population genotypings.

## Note to MacOs users: 
You need to allow unsigned application to run.

1. Upload the zipped package **GenAPoPop1.0_mac** from **Gitlab**
2. Unzip
3. Open a **Terminal** in the right unzipped path (the folder where GenAPoPop executable can be found)
3. Enter this command in **Terminal**: _chmod 755 GenAPoPop_
4. Launch a first time genapopop entering in **Terminal** this command: _./GenAPoPop_
It will fail for security reason: unsigned apple executable need to be allowed
5. Click on finder and open **Preferences>Security & Privacy>General** in this tab (**General**) at the bottom you will see that GenAPoPop was listed the first time you launch it as unsecured. Click on “**allow**” button
6. Now, go back to the Terminal and enter again: _./GenAPoPop_
This time, **GenAPoPop** will work on all next launch on your Mac. Wait after ~30s to couple of minutes (time for the program to unpack all tools needed) to see the GenAPoPop window opening.



# Toy datasets
In our manuscript, we used four pseudo-observed datasets (simulated), one real SNP tetraploid dataset from a _Ludwigia grandiflora subs hexapetala_'s population (a plant) and one real microsatelitte tetraploid dataset from an _Aulactinia stella_'s population (a sea-anemone). these dataserts cn be used to test GenAPoPop and used for consistency tests with previous and future sofwares. 

These datasets are packaged in the file "Barloy_et_al._2022.zip". These datasets are deposited on Zenodo and can be cited using the following DOI: Barloy et al. 2022.

Barloy, D., Bocharova, E., Harang, M., Portillo, L., & Stoeckel, S. (2022). Reference datasets for consistency tests of GENAPOPOP 1.0 software: a user-friendly software to analyse genetic diversity and structure in partially clonal and selfed polyploid organisms. (1.0) [Data set]. Zenodo. https://doi.org/10.5281/zenodo.8164531


# Know issue
Linux systems using wayland have to first enter the following command-line in a terminal to be able to launch properly GenAPoPop:
> export QT_QPA_PLATFORM=wayland

This is an old Qt issue still running in multiple linux system using wayland.

# Contact: 
INRAE/Institut Agro/Université Rennes1

Institute for Genetics, Environment and Plant Protection, UMR 1349

Team DEMEcology: Dynamics, Evolution, Modelling and Ecology

Domaine de la Motte, BP 35327, 35653 Le Rheu cedex, France

solenn.stoeckel@inrae.fr


## Last update: 19 July 2023, Solenn Stoeckel.



